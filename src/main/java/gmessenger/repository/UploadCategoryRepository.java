package gmessenger.repository;

import gmessenger.entity.UploadCategory;
import org.springframework.data.repository.CrudRepository;

public interface UploadCategoryRepository extends CrudRepository<UploadCategory, Integer> {

}
