package gmessenger.repository;

import gmessenger.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;
import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Integer> {
    /**
     * Finds {@User}s by login or email
     *
     * @param login String
     * @param email String
     *
     * @return User[]
     */
    public ArrayList<User> findALLByLoginOrEmail(String login, String email);

    /**
     * Finds {@User} by login
     *
     * @param login String
     * @return User
     */
    public Optional<User> findByLogin(String login);
}
