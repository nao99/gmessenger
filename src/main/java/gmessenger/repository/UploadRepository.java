package gmessenger.repository;

import gmessenger.entity.Upload;
import org.springframework.data.repository.CrudRepository;

public interface UploadRepository extends CrudRepository<Upload, Integer> {

}
