package gmessenger.repository;

import gmessenger.entity.UserBlacklist;
import org.springframework.data.repository.CrudRepository;

public interface UserBlacklistRepository extends CrudRepository<UserBlacklist, Integer> {

}
