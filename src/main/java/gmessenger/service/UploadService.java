package gmessenger.service;

import gmessenger.entity.Upload;
import gmessenger.entity.UploadCategory;
import gmessenger.exception.UploadBadRequestException;
import gmessenger.exception.UploadCategoryInternalErrorException;
import gmessenger.exception.UploadInternalErrorException;
import gmessenger.repository.UploadCategoryRepository;
import gmessenger.repository.UploadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Class UploadService
 *
 * Filesystem structure looks like this: audio => [year => month => day => file1, file2, etc]. The same for video, documents, etc
 */
@Service
public class UploadService {
    /**
     * @var uploadCategoryRepository UploadCategoryRepository
     */
    private UploadCategoryRepository uploadCategoryRepository;

    /**
     * @var uploadCategoryRepository UploadRepository
     */
    private UploadRepository uploadRepository;

    /**
     * UploadService constructor
     *
     * @param uploadCategoryRepository UploadCategoryRepository
     * @param uploadRepository         UploadRepository
     */
    @Autowired
    public UploadService(UploadCategoryRepository uploadCategoryRepository, UploadRepository uploadRepository) {
        this.uploadCategoryRepository = uploadCategoryRepository;
        this.uploadRepository = uploadRepository;
    }

    /**
     * Creates md5 hash string
     *
     * @param commonStr String
     * @return String
     */
    private String getMd5HashStr(String commonStr) {
        return DigestUtils.md5DigestAsHex(commonStr.getBytes());
    }

    //TODO [Nikolai - 02.12.19]: Improve this class (all methods). The same create a methods like delete, etc.

    /**
     * Creates a folder hierarchy on the server
     *
     * @param destination String
     */
    private void createFolderHierarchyByPath(String destination) {
        String[] folders = destination.split("/");

        destination = "";
        for (String folder : folders) {
            destination += folder + "/";

            if (!folder.equals("") && !folder.equals(Upload.MAIN_FOLDER_NAME)) {
                File dir = new File(destination);
                if (!dir.exists()) {
                    boolean isCreated = dir.mkdir();
                    if (!isCreated) {
                        throw new UploadInternalErrorException("Directory cannot be created");
                    }
                }
            }
        }
    }

    /**
     * Saves {@MultipartFile} on the local storage
     *
     * @param file MultipartFile
     * @return File
     * */
    public File saveFileOnTheStorage(MultipartFile file) throws IOException {
        if (file.isEmpty()) {
            throw new UploadBadRequestException("File cannot be empty");
        }

        String fileType = file.getContentType();
        if (fileType == null) {
            throw new UploadBadRequestException("File type cannot be null");
        }

        String fileDestination = null;
        String fileDirectory = this.getMd5HashStr(file.getName());
        LocalDate localDate = LocalDate.now();

        switch (fileType) {
            case Upload.AUDIO_MPEG_FILE_TYPE:
                fileDestination = Upload.AUDIO_FOLDER_PATH;
                break;
            case Upload.VIDEO_MP4_FILE_TYPE:
                fileDestination = Upload.VIDEO_FOLDER_PATH;
                break;
            case Upload.IMAGE_JPEG_FILE_TYPE:
            case Upload.IMAGE_GIF_FILE_TYPE:
            case Upload.IMAGE_PNG_FILE_TYPE:
                fileDestination = Upload.IMAGE_FOLDER_PATH;
                break;
            case Upload.TEXT_TXT_FILE_TYPE:
            case Upload.DOCUMENT_PDF_FILE_TYPE:
            case Upload.DOCUMENT_ZIP_FILE_TYPE:
            case Upload.DOCUMENT_MSWORD_FILE_TYPE:
                fileDestination = Upload.DOCUMENT_FOLDER_PATH;
                break;
        }

        fileDestination += localDate.getYear() + "/" + localDate.getMonthValue() + "/" + localDate.getDayOfMonth() + "/" + fileDirectory;

        this.createFolderHierarchyByPath(fileDestination);

        fileDestination +=  "/" + file.getOriginalFilename().replace(" ", "");

        File savedFile = new File(fileDestination);
        if (savedFile.exists()) {
            throw new UploadBadRequestException("File already exist");
        }

        boolean isFileCreated = savedFile.createNewFile();
        if (!isFileCreated) {
            throw new UploadInternalErrorException("File wasn't created. An error has occurred");
        }

        file.transferTo(savedFile);

        return savedFile;
    }

    /**
     * Creates {@Upload} by url on the file
     *
     * @param url String
     * @return Upload
     */
    public Upload createByUrl(String url) {
        Optional<UploadCategory> uploadCategory = this.uploadCategoryRepository.findById(UploadCategory.FILE_PNG);
        if (!uploadCategory.isPresent()) {
            throw new UploadCategoryInternalErrorException("Upload category not found");
        }

        Upload upload = Upload.builder()
                .uploadCategory(uploadCategory.get())
                .path("test")
                .name("hello")
                .extension("png")
                .size(123)
                .createdAt(new Timestamp(System.currentTimeMillis()).getTime())
                .updatedAt(new Timestamp(System.currentTimeMillis()).getTime())
                .build();

        uploadRepository.save(upload);

        return upload;
    }
}
