package gmessenger.service;

import gmessenger.dto.UserCreateDto;
import gmessenger.entity.Role;
import gmessenger.entity.Upload;
import gmessenger.entity.User;
import gmessenger.entity.UserRole;
import gmessenger.exception.RoleInternalServerErrorException;
import gmessenger.exception.UserBadRequestException;
import gmessenger.exception.UserNotFoundException;
import gmessenger.repository.RoleRepository;
import gmessenger.repository.UserRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Class UserService
 */
@Service
public class UserService {
    /**
     * @var EntityManager
     */
    private EntityManager em;

    /**
     * @var UploadService
     */
    private UploadService uploadService;

    /**
     * @var UserRepository
     */
    private UserRepository userRepository;

    /**
     * @var RoleRepository
     */
    private RoleRepository roleRepository;

    /**
     * UserService constructor
     *
     * @param entityManagerFactory EntityManager
     * @param uploadService        UploadService
     * @param userRepository       UserRepository
     * @param roleRepository       RoleRepository
     */
    public UserService(EntityManagerFactory entityManagerFactory, UploadService uploadService, UserRepository userRepository, RoleRepository roleRepository) {
        this.em = entityManagerFactory.createEntityManager();
        this.uploadService = uploadService;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    /**
     * Creates new {@User} by {@UserCreateDto}
     *
     * @param userDto UserCreateDto
     * @return User
     */
    public User createUser(UserCreateDto userDto) {
        List<User> existedUsers = userRepository.findALLByLoginOrEmail(userDto.getLogin(), userDto.getEmail());

        if (!existedUsers.isEmpty()) {
            throw new UserBadRequestException("User with the same login or email already exist");
        }

        User user = null;

        try {
            em.getTransaction().begin();

            Upload avatar = null;
            //TODO: Need to describe and does not include uri (describe MediaService)
            //if (userDto.getAvatarUrl() != null) {
            //    avatar = this.uploadService.createByUrl(userDto.getAvatarUrl());
            //}

            user = User.builder()
                    .avatar(avatar)
                    .name(userDto.getName())
                    .email(userDto.getEmail())
                    .login(userDto.getLogin())
                    .password(new BCryptPasswordEncoder().encode(userDto.getPassword()))
                    .localPassword(userDto.getLocalPassword())
                    .about(userDto.getAbout())
                    .isEnabled(false)
                    .createdAt(new Timestamp(System.currentTimeMillis()).getTime())
                    .updatedAt(new Timestamp(System.currentTimeMillis()).getTime())
                    .build();

            Optional<Role> role = this.roleRepository.findById(Role.ROLE_USER_ID);
            if (!role.isPresent()) {
                throw new RoleInternalServerErrorException("User role does not exist");
            }

            List<UserRole> roles = new ArrayList<>();
            UserRole userRole = UserRole.builder()
                    .user(user)
                    .role(role.get())
                    .build();

            roles.add(userRole);
            user.setRoles(roles);

            //this.em.persist(avatar);
            this.em.persist(user);
            this.em.persist(userRole);

            this.em.flush();
            this.em.getTransaction().commit();
        } catch (Exception e) {
            this.em.getTransaction().rollback();

            throw e;
        }

        return user;
    }

    /**
     * Gets {@User} by username
     *
     * @param login String
     * @return User
     */
    public User getUserByLogin(String login) {
        Optional<User> user = this.userRepository.findByLogin(login);
        if (!user.isPresent()) {
            throw new UserNotFoundException("User by login" + login + " not found");
        }

        return user.get();
    }
}
