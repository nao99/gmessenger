package gmessenger.service;

import gmessenger.dto.JwtTokenCreateDto;
import gmessenger.entity.User;
import gmessenger.exception.AuthenticationBadRequestException;
import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.TextCodec;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Class JwtTokenService
 */
@Service
public class JwtTokenService {
    private static final int JWT_TOKEN_EXPIRATION_TIME = 30 * 24 * 60 * 60;
    private static final String JWT_TOKEN_SECRET_KEY   = "GMessemg@r_secret_312";
    private static final String JWT_TOKEN_SUBJECT      = "gmessenger_api";

    public static final String JWT_TOKEN_PREFIX = "Bearer";
    public static final String JWT_TOKEN_HEADER = "Authorization";

    /**
     * @var UserService
     */
    private UserService userService;

    /**
     * JwtTokenService constructor
     *
     * @param userService UserService
     */
    public JwtTokenService(UserService userService) {
        this.userService = userService;
    }

    /**
     * Gets {@User} login by JwtToken
     *
     * @param token String
     * @return String
     */
    public String getUserLoginByJwtToken(String token) {
        Jws<Claims> jws = this.getJwtPayloadByJwtToken(token);

        return jws.getBody().get("login", String.class);
    }

    /**
     * Gets JwtToken by username and password
     *
     * @param userLoginQueryDto JwtTokenCreateDto
     * @return String
     */
    public String getJwtToken(JwtTokenCreateDto userLoginQueryDto) {
        if (userLoginQueryDto.getLogin() == null || userLoginQueryDto.getPassword() == null) {
            throw new AuthenticationBadRequestException("Login or password cannot be empty!");
        }

        User user = this.userService.getUserByLogin(userLoginQueryDto.getLogin());
        if (!new BCryptPasswordEncoder().matches(userLoginQueryDto.getPassword(), user.getPassword())) {
            throw new AuthenticationBadRequestException("Password or login is wrong!");
        }

        return this.createJwtToken(user);
    }

    /**
     * Creates JwtToken
     *
     * @param user User
     * @return String
     */
    private String createJwtToken(User user) {
        Map<String, Object> tokenData = new HashMap<>();

        tokenData.put("clientType", "user");
        tokenData.put("userID", user.getId().toString());
        tokenData.put("login", user.getLogin());
        tokenData.put("tokenCreateDate", new Date().toString());
        tokenData.put("tokenExpirationDate", String.valueOf(JWT_TOKEN_EXPIRATION_TIME));

        return Jwts.builder()
                .setSubject(JWT_TOKEN_SUBJECT)
                .setExpiration(new Date(JWT_TOKEN_EXPIRATION_TIME))
                .setClaims(tokenData)
                .signWith(SignatureAlgorithm.HS512, TextCodec.BASE64.decode(JWT_TOKEN_SECRET_KEY))
                .compact();
    }

    /**
     * Gets payload from JwtToken
     *
     * @param token String
     * @return Jws
     */
    private Jws<Claims> getJwtPayloadByJwtToken(String token) {
        return Jwts.parser()
                .setSigningKey(TextCodec.BASE64.decode(JWT_TOKEN_SECRET_KEY))
                .parseClaimsJws(token);
    }
}
