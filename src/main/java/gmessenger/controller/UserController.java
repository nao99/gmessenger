package gmessenger.controller;

import gmessenger.dto.UserCreateDto;
import gmessenger.dto.UserDto;
import gmessenger.entity.User;
import gmessenger.mapper.UserMapper;
import gmessenger.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.*;
import java.util.Map;
import java.util.Set;

/**
 * Class UserController
 */
@RestController
@RequestMapping(value = "/gmessenger/api/v1.0/user")
public class UserController {
    /**
     * @var UserService
     */
    private UserService userService;

    /**
     * @var Validator
     */
    private Validator validator;

    /**
     * UserController constructor
     *
     * @param userService      UserService
     * @param validatorFactory ValidatorFactory
     */
    public UserController(UserService userService, ValidatorFactory validatorFactory) {
        this.userService = userService;
        this.validator = validatorFactory.getValidator();
    }

    /**
     * Creates (registers) {@User}
     *
     * @param request HttpServletRequest
     * @return ResponseEntity
     */
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<UserDto> createUser(HttpServletRequest request) {
        UserCreateDto data = new UserCreateDto(request);

        Set<ConstraintViolation<UserCreateDto>> violations = this.validator.validate(data);
        if (!violations.isEmpty()) {
            throw new ValidationException(violations.toString());
        }

        User user = this.userService.createUser(data);
        UserDto userJson = UserMapper.userToUserDto(user);

        return new ResponseEntity<UserDto>(userJson, HttpStatus.OK);
    }

    /**
     * Removes {@User}
     *
     * @param userId int
     * @return ResponseEntity
     */
    @RequestMapping(value = "/{id}/", method = RequestMethod.DELETE)
    public ResponseEntity deleteUser(@PathVariable("id") int userId) {
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
