package gmessenger.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * Class UserDto
 */
@Data
@AllArgsConstructor
public class UserDto {
    /**
     * @var Integer
     */
    private Integer id;

    /**
     * @var String
     */
    private String name;

    /**
     * @var String
     */
    private String email;

    /**
     * @var String
     */
    private String login;

    /**
     * @var String
     */
    private String about;

    /**
     * @var String
     */
    private String avatarUri;

    /**
     * @var List<String>
     */
    private List<String> roles;

    /**
     * @var boolean
     */
    private boolean isEnabled = false;

    /**
     * @var long
     */
    private long createdAt;

    /**
     * @var long
     */
    private long updatedAt;
}
