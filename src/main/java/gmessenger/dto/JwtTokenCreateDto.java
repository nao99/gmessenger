package gmessenger.dto;

import lombok.Data;

import javax.servlet.http.HttpServletRequest;

/**
 * Class UserLoginQueryDto
 */
@Data
public class JwtTokenCreateDto {
    /**
     * @var String
     */
    private String login;

    /**
     * @var String
     */
    private String password;

    /**
     * UserLoginQueryDto constructor
     *
     * @param request HttpServletRequest //TODO: Need change from HttpServletRequest to Map
     */
    public JwtTokenCreateDto(HttpServletRequest request) {
        this.login = request.getParameter("login");
        this.password = request.getParameter("password");
    }
}
