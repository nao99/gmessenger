package gmessenger.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Class JwtTokenDto
 */
@Data
public class JwtTokenDto implements Serializable {
    /**
     * @var String jwtToken
     */
    private String jwtToken;

    /**
     * JwtTokenDto constructor
     */
    public JwtTokenDto(String jwtToken) {
        this.jwtToken = jwtToken;
    }
}
