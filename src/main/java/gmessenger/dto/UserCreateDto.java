package gmessenger.dto;

import lombok.Data;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.*;

/**
 * Class UserCreateDto
 */
@Data
public class UserCreateDto {
    /**
     * @var String
     */
    //@Pattern(regexp = "^https://www\\.[a-z0-9+.-]+$") //TODO: Need to describe a right regexp
    private String avatarUrl;

    /**
     * @var String
     */
    @NotNull(message = "Name cannot be empty")
    private String name;

    /**
     * @var String
     */
    @Email(message = "It's not email")
    @NotNull(message = "Email cannot be empty")
    private String email;

    /**
     * @var String
     */
    @NotNull(message = "Login cannot by empty")
    private String login;

    /**
     * @var String
     */
    @Pattern(regexp = "^(?=.*[A-Z].*[A-Z])(?=.*[!@#$&*])(?=.*[0-9].*[0-9])(?=.*[a-z].*[a-z].*[a-z]).{8}$", message = "Password should contains 3 a-z, 2 A-Z, 2 0-9 and 1 !-*")
    @NotNull(message = "Password cannot be empty")
    private String password;

    /**
     * @var String
     */
    private String localPassword;

    /**
     * @var String
     */
    private String about;

    /**
     * UserCreateDto constructor
     */
    public UserCreateDto(HttpServletRequest request) {
        this.avatarUrl = request.getParameter("avatarUrl");
        this.name = request.getParameter("name");
        this.email = request.getParameter("email");
        this.login = request.getParameter("login");
        this.password = request.getParameter("password");
        this.localPassword = request.getParameter("localPassword");
        this.about = request.getParameter("about");
    }
}
