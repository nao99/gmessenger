package gmessenger.config;

import gmessenger.entity.User;
import gmessenger.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Class UserPrincipalDetailsService
 */
@Service
public class UserPrincipalDetailsService implements UserDetailsService {
    /**
     * @var UserRepository
     */
    private UserRepository userRepository;

    /**
     * UserPrincipalDetailsService constructor
     */
    public UserPrincipalDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Loads {@User} by username (login)
     *
     * @param s String
     * @return UserPrincipal
     */
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Optional<User> user = this.userRepository.findByLogin(s);
        if (!user.isPresent()) {
            throw new UsernameNotFoundException("User by login" + s + " not found");
        }

        return new UserPrincipal(user.get());
    }
}