package gmessenger.config;

import gmessenger.dto.JwtTokenCreateDto;
import gmessenger.service.JwtTokenService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Class TokenAuthenticationFilter
 */
public class TokenAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    /**
     * @var JwtTokenCreateDto
     */
    private JwtTokenCreateDto jwtTokenCreateDto;

    /**
     * @var JwtTokenService
     */
    private JwtTokenService jwtTokenService;

    /**
     * @var AuthenticationManager
     */
    private AuthenticationManager authenticationManager;

    /**
     * TokenAuthenticationFilter constructor
     *
     * @param authenticationManager AuthenticationManager
     * @param jwtTokenService       JwtTokenService
     */
    public TokenAuthenticationFilter(AuthenticationManager authenticationManager, JwtTokenService jwtTokenService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenService = jwtTokenService;
    }

    /**
     * Attempts authenticate
     *
     * @param httpServletRequest  HttpServletRequest
     * @param httpServletResponse HttpServletResponse
     *
     * @return Authentication
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws AuthenticationException {
        this.jwtTokenCreateDto = new JwtTokenCreateDto(httpServletRequest);
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(this.jwtTokenCreateDto.getLogin(), this.jwtTokenCreateDto.getPassword(), new ArrayList<>());

        return authenticationManager.authenticate(authenticationToken);
    }

    /**
     * Triggers if {@User} was successfully authenticating
     *
     * @param httpServletRequest  HttpServletRequest
     * @param httpServletResponse HttpServletResponse
     * @param filterChain         FilterChain
     * @param authentication      Authentication
     *
     * @throws IOException, ServletException
     */
    @Override
    protected void successfulAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain, Authentication authentication) throws IOException, ServletException {
        String token = this.jwtTokenService.getJwtToken(this.jwtTokenCreateDto);

        httpServletResponse.addHeader(JwtTokenService.JWT_TOKEN_HEADER, JwtTokenService.JWT_TOKEN_PREFIX + token);
    }
}
