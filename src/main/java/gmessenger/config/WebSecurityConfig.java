package gmessenger.config;

import gmessenger.service.JwtTokenService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Class WebSecurityConfig
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    /**
     * @var JwtTokenService
     */
    private JwtTokenService jwtTokenService;

    /**
     * @var UserPrincipalDetailsService
     */
    private UserPrincipalDetailsService userPrincipalDetailsService;

    /**
     * @var PasswordEncoder
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * WebSecurityConfig constructor
     *
     * @param jwtTokenService             JwtTokenService
     * @param userPrincipalDetailsService UserPrincipalDetailsService
     */
    public WebSecurityConfig(JwtTokenService jwtTokenService, UserPrincipalDetailsService userPrincipalDetailsService) {
        this.jwtTokenService = jwtTokenService;
        this.userPrincipalDetailsService = userPrincipalDetailsService;
    }

    /**
     * Configures authentication and authorization
     *
     * @param httpSecurity HttpSecurity
     */
    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                    .addFilter(new TokenAuthenticationFilter(authenticationManager(), this.jwtTokenService))
                    .addFilter(new TokenAuthorizationFilter(authenticationManager(), this.jwtTokenService, this.userPrincipalDetailsService))
                .authorizeRequests()
                .antMatchers("/", "/gmessenger/api/v1.0/user/").permitAll()
                .anyRequest().authenticated();
    }

    /**
     * Configures global
     *
     * @param authenticationManagerBuilder AuthenticationManagerBuilder
     */
    protected void configureGlobal(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder
                .userDetailsService(this.userPrincipalDetailsService)
                .passwordEncoder(this.passwordEncoder());
    }
}
