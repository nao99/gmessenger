package gmessenger.config;

import gmessenger.service.JwtTokenService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Class TokenAuthorizationFilter
 */
public class TokenAuthorizationFilter extends BasicAuthenticationFilter {
    /**
     * @var JwtTokenService
     */
    private JwtTokenService jwtTokenService;

    /**
     * @var UserService
     */
    private UserPrincipalDetailsService userPrincipalDetailsService;

    /**
     * TokenAuthorizationFilter constructor
     *
     * @param authenticationManager       AuthenticationManager
     * @param jwtTokenService             JwtTokenService
     * @param userPrincipalDetailsService UserPrincipalDetailsService
     */
    public TokenAuthorizationFilter(AuthenticationManager authenticationManager, JwtTokenService jwtTokenService, UserPrincipalDetailsService userPrincipalDetailsService) {
        super(authenticationManager);
        this.jwtTokenService = jwtTokenService;
        this.userPrincipalDetailsService = userPrincipalDetailsService;
    }

    /**
     * Does authentication
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @param chain    FilterChain
     * @throws IOException, ServletException
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String header = request.getHeader(JwtTokenService.JWT_TOKEN_HEADER);

        if (header == null || !header.startsWith(JwtTokenService.JWT_TOKEN_PREFIX)) {
            chain.doFilter(request, response);

            return;
        }

        UsernamePasswordAuthenticationToken authentication = this.getUsernamePasswordAuthenticationToken(request);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        chain.doFilter(request, response);
    }

    /**
     * Gets {@UsernamePasswordAuthenticationToken}
     *
     * @param request HttpServletRequest
     * @return UsernamePasswordAuthenticationToken
     */
    private UsernamePasswordAuthenticationToken getUsernamePasswordAuthenticationToken(HttpServletRequest request) {
        String token = request
                .getHeader(JwtTokenService.JWT_TOKEN_HEADER)
                .replace(JwtTokenService.JWT_TOKEN_PREFIX, "");

        if (token == null) {
            return null;
        }

        String login = this.jwtTokenService.getUserLoginByJwtToken(token);
        if (login == null) {
            return null;
        }

        UserPrincipal principal = (UserPrincipal) this.userPrincipalDetailsService.loadUserByUsername(login);

        return new UsernamePasswordAuthenticationToken(login, null, principal.getAuthorities());
    }
}
