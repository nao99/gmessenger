package gmessenger.config;

import gmessenger.entity.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Class UserPrincipal
 */
public class UserPrincipal implements UserDetails {
    /**
     * @var User
     */
    private User user;

    /**
     * UserPrincipal constructor
     *
     * @param user User
     */
    public UserPrincipal(User user) {
        this.user = user;
    }

    /**
     * Gets {@UserRole}s like {@GrantedAuthority}s
     *
     * @return GrantedAuthority[]
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();

        this.user.getRoles().forEach(p -> {
            GrantedAuthority authority = new SimpleGrantedAuthority(p.getRole().getName());
            authorities.add(authority);
        });

        return authorities;
    }

    /**
     * Gets {@User} password
     */
    @Override
    public String getPassword() {
        return this.user.getPassword();
    }

    /**
     * Gets {@User} login
     */
    @Override
    public String getUsername() {
        return this.user.getLogin();
    }

    /**
     * Checks {@User} on account non expired
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * Checks {@User} on account non locked
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * Checks {@User} on credentials
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * Checks {@User} on enabled
     */
    @Override
    public boolean isEnabled() {
        return this.user.isEnabled();
    }
}
