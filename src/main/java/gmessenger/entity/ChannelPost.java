package gmessenger.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Class ChannelPost
 */
@Entity
@Data
@Table(name = "channel_post")
public class ChannelPost {
    /**
     * @var Integer
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * @var Channel
     */
    @ManyToOne
    @JoinColumn(name = "channel_id", nullable = false)
    private Channel channel;

    /**
     * @var Upload
     */
    @Column(name = "content", length = 4096, nullable = false)
    private String content;

    /**
     * @var long
     */
    @Column(name = "created_at", nullable = false)
    private long createdAt;

    /**
     * @var long
     */
    @Column(name = "updated_at", nullable = false)
    private long updatedAt;

    /**
     * @var ChannelPostAttachment[]
     */
    @OneToMany(mappedBy = "channelPost")
    private List<ChannelPostAttachment> attachments = new ArrayList<ChannelPostAttachment>();
}
