package gmessenger.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Class Channel
 */
@Entity
@Data
@Table(name = "channel")
public class Channel {
    /**
     * @var Integer
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * @var ChannelCustomization
     */
    @OneToOne
    @JoinColumn(name = "channel_customization_id")
    private ChannelCustomization channelCustomization;

    /**
     * @var long
     */
    @Column(name = "created_at", nullable = false)
    private long createdAt;
}
