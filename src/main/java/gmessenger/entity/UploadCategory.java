package gmessenger.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Class UploadCategory
 */
@Entity
@Data
@Table(name = "upload_category")
public class UploadCategory {
    public static final int FILE_PNG = 1;

    /**
     * @var Integer
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * @var String
     */
    @Column(name = "name", length = 65, nullable = false)
    private String name;
}
