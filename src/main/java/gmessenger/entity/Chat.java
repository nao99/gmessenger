package gmessenger.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * @var Class Chat
 */
@Entity
@Data
@Table(name = "chat")
public class Chat {
    /**
     * @var Integer
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * @var User
     */
    @ManyToOne
    @JoinColumn(name = "creator_id", nullable = false)
    private User creator;

    /**
     * @var long
     */
    @Column(name = "created_at", nullable = false)
    private long createdAt;
}
