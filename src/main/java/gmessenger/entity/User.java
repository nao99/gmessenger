package gmessenger.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

/**
 * Class User
 */
@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user")
public class User {
    /**
     * @var Integer
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * @var Upload
     */
    @OneToOne
    @JoinColumn(name = "avatar_id")
    private Upload avatar;

    /**
     * @var String
     */
    @Column(name = "name", length = 65, nullable = false)
    private String name;

    /**
     * @var String
     */
    @Column(name = "email", length = 65, nullable = false)
    private String email;

    /**
     * @var String
     */
    @Column(name = "login", length = 65, nullable = false)
    private String login;

    /**
     * @var String
     */
    @Column(name = "password", length = 32, nullable = false)
    private String password;

    /**
     * @var String
     */
    @Column(name = "local_password", length = 8)
    private String localPassword;

    /**
     * @var String
     */
    @Column(name = "about", length = 1024)
    private String about;

    /**
     * @var boolean
     */
    @Column(name = "is_enabled", nullable = false)
    private boolean isEnabled = false;

    /**
     * @var long
     */
    @Column(name = "created_at", nullable = false)
    private long createdAt;

    /**
     * @var long
     */
    @Column(name = "updated_at", nullable = false)
    private long updatedAt;

    /**
     * @var UserRole[]
     */
    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    private List<UserRole> roles;
}
