package gmessenger.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Class ChatMemberMessage
 */
@Entity
@Data
@Table(name = "chat_member_message")
public class ChatMemberMessage {
    /**
     * @var Integer
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * @var Chat
     */
    @ManyToOne
    @JoinColumn(name = "chat_id", nullable = false)
    private Chat chat;

    /**
     * @var ChatMember
     */
    @ManyToOne
    @JoinColumn(name = "chat_member_id", nullable = false)
    private ChatMember chatMember;

    /**
     * @var String
     */
    @Column(name = "content", length = 4096, nullable = false)
    private String content;

    /**
     * @var long
     */
    @Column(name = "created_at", nullable = false)
    private long createdAt;

    /**
     * @var long
     */
    @Column(name = "updated_at", nullable = false)
    private long updatedAt;
}
