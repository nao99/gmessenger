package gmessenger.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Class Role.
 * Keeps User roles (ROLE_ADMIN, ROLE_USER, etc)
 */
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "role")
public class Role {
    public static final int ROLE_ADMIN_ID  = 1;
    public static final int ROLE_USER_ID   = 2;
    public static final int ROLE_BANNED_ID = 3;

    /**
     * @var Integer
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * @var String
     */
    @Column(name = "name", length = 65, nullable = false)
    private String name;
}
