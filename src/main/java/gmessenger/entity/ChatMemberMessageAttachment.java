package gmessenger.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Class ChatMemberMessageAttachment
 */
@Entity
@Data
@Table(name = "chat_member_message_attachment")
public class ChatMemberMessageAttachment {
    /**
     * @var Integer
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * @var ChatMemberMessage
     */
    @ManyToOne
    @JoinColumn(name = "chat_member_message_id", nullable = false)
    private ChatMemberMessage chatMemberMessage;

    /**
     * @var Upload
     */
    @OneToOne
    @JoinColumn(name = "attachment_id", nullable = false)
    private Upload attachment;
}
