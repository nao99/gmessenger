package gmessenger.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Class ChannelCustomization
 */
@Entity
@Data
@Table(name = "channel_customization")
public class ChannelCustomization {
    /**
     * @var Integer
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * @var Upload
     */
    @OneToOne
    @JoinColumn(name = "image_id")
    private Upload image;

    /**
     * @var String
     */
    @Column(name = "description", length = 1024)
    private String description;

    /**
     * @var long
     */
    @Column(name = "created_at", nullable = false)
    private long createdAt;

    /**
     * @var long
     */
    @Column(name = "updated_at", nullable = false)
    private long updatedAt;
}
