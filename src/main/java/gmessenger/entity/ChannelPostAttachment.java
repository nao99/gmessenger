package gmessenger.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Class ChannelPostAttachment
 */
@Entity
@Data
@Table(name = "channel_post_attachment")
public class ChannelPostAttachment {
    /**
     * @var Integer
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * @var ChannelPost
     */
    @ManyToOne
    @JoinColumn(name = "channel_post_id", nullable = false)
    private ChannelPost channelPost;

    /**
     * @var Upload
     */
    @OneToOne
    @JoinColumn(name = "attachment_id", nullable = false)
    private Upload attachment;
}
