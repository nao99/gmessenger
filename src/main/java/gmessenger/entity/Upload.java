package gmessenger.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Class Upload
 */
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "upload")
public class Upload {
    public static final String MAIN_FOLDER_NAME = "data";

    public static final String AUDIO_FOLDER_PATH    = "/data/audio/";
    public static final String VIDEO_FOLDER_PATH    = "/data/video/";
    public static final String DOCUMENT_FOLDER_PATH = "/data/document/";
    public static final String IMAGE_FOLDER_PATH    = "/data/image/";

    public static final String AUDIO_MPEG_FILE_TYPE  = "audio/mpeg";

    public static final String VIDEO_MP4_FILE_TYPE = "video/mp4";

    public static final String IMAGE_JPEG_FILE_TYPE = "image/jpeg";
    public static final String IMAGE_GIF_FILE_TYPE  = "image/gif";
    public static final String IMAGE_PNG_FILE_TYPE  = "image/png";

    public static final String TEXT_TXT_FILE_TYPE = "text/plain";

    public static final String DOCUMENT_PDF_FILE_TYPE    = "application/pdf";
    public static final String DOCUMENT_ZIP_FILE_TYPE    = "application/zip";
    public static final String DOCUMENT_MSWORD_FILE_TYPE = "application/msword";

    /**
     * @var Integer
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * @var UploadCategory
     */
    @ManyToOne
    @JoinColumn(name = "upload_category_id", nullable = false)
    private UploadCategory uploadCategory;

    /**
     * @var String
     */
    @Column(name = "path", nullable = false)
    private String path;

    /**
     * @var String
     */
    @Column(name = "name", length = 65, nullable = false)
    private String name;

    /**
     * @var String
     */
    @Column(name = "extension", length = 8, nullable = false)
    private String extension;

    /**
     * @var Integer
     */
    @Column(name = "size", nullable = false)
    private Integer size;

    /**
     * @var long
     */
    @Column(name = "created_at", nullable = false)
    private long createdAt;

    /**
     * @var long
     */
    @Column(name = "updated_at", nullable = false)
    private long updatedAt;
}
