package gmessenger.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Class ChatMember
 */
@Entity
@Data
@Table(name = "chat_member")
public class ChatMember {
    /**
     * @var Integer
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * @var Chat
     */
    @ManyToOne
    @JoinColumn(name = "chat_id", nullable = false)
    private Chat chat;

    /**
     * @var User
     */
    @ManyToOne
    @JoinColumn(name = "member_id", nullable = false)
    private User member;

    /**
     * @var long
     */
    @Column(name = "joined_at", nullable = false)
    private long joinedAt;
}
