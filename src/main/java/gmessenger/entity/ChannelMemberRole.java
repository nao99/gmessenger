package gmessenger.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Class ChannelMemberRole
 */
@Entity
@Data
@Table(name = "channel_member_role")
public class ChannelMemberRole {
    /**
     * @var Integer
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * @var String
     */
    @Column(name = "name", length = 65, nullable = false)
    private String name;
}
