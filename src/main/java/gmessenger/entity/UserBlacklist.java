package gmessenger.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Class UserBlacklist
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user_blacklist")
public class UserBlacklist {
    /**
     * @var Integer
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * @var User
     */
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    /**
     * @var User
     */
    @ManyToOne
    @JoinColumn(name = "user_blocked_id", nullable = false)
    private User userBlocked;

    /**
     * @var long
     */
    @Column(name = "blocked_at", nullable = false)
    private long blockedAt;
}
