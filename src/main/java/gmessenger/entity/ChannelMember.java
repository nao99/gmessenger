package gmessenger.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Class ChannelMember
 */
@Entity
@Data
@Table(name = "channel_member")
public class ChannelMember {
    /**
     * @var Integer
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * @var Channel
     */
    @ManyToOne
    @JoinColumn(name = "channel_id", nullable = false)
    private Channel channel;

    /**
     * @var User
     */
    @ManyToOne
    @JoinColumn(name = "member_id", nullable = false)
    private User member;

    /**
     * @var ChannelMemberRole
     */
    @ManyToOne
    @JoinColumn(name = "member_role_id", nullable = false)
    private ChannelMemberRole memberRole;

    /**
     * @var long
     */
    @Column(name = "created_at", nullable = false)
    private long createdAt;

    /**
     * @var long
     */
    @Column(name = "updated_at", nullable = false)
    private long updatedAt;
}
