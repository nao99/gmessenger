package gmessenger.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Class UploadInternalErrorException
 */
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class UploadInternalErrorException extends RuntimeException {
    /**
     * UploadInternalErrorException constructor
     *
     * @param errorMessage String
     */
    public UploadInternalErrorException(String errorMessage) {
        super(errorMessage);
    }
}
