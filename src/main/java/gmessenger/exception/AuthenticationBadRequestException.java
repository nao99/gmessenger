package gmessenger.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Class AuthenticationBadRequestException
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class AuthenticationBadRequestException extends RuntimeException {
    /**
     * AuthenticationBadRequestException constructor
     *
     * @param message String
     */
    public AuthenticationBadRequestException(String message) {
        super(message);
    }
}
