package gmessenger.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Class UploadCategoryInternalErrorException
 */
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class UploadCategoryInternalErrorException extends RuntimeException {
    /**
     * UploadCategoryInternalErrorException constructor
     *
     * @param message String
     */
    public UploadCategoryInternalErrorException(String message) {
        super(message);
    }
}
