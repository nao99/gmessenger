package gmessenger.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Class UserNotFoundException
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserNotFoundException extends RuntimeException {
    /**
     * UserNotFoundException constructor
     *
     * @param message String
     */
    public UserNotFoundException(String message) {
        super(message);
    }
}
