package gmessenger.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Class RoleInternalServerErrorException
 */
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class RoleInternalServerErrorException extends RuntimeException {
    /**
     * RoleInternalServerErrorException constructor
     *
     * @param errorMessage String
     */
    public RoleInternalServerErrorException(String errorMessage) {
        super(errorMessage);
    }
}
