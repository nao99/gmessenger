package gmessenger.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Class UploadBadRequestException
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UploadBadRequestException extends RuntimeException {
    /**
     * UploadBadRequestException constructor
     *
     * @param errorMessage String
     */
    public UploadBadRequestException(String errorMessage) {
        super(errorMessage);
    }
}
