package gmessenger.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Class UserBadRequestException
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UserBadRequestException extends RuntimeException {
    /**
     * UserBadRequestException constructor
     *
     * @param message String
     */
    public UserBadRequestException(String message) {
        super(message);
    }
}
