SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

CREATE TABLE `channel`
(
    `id`                       int(11)    NOT NULL,
    `channel_customization_id` int(11) DEFAULT NULL,
    `created_at`               bigint(20) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = cp1251;

-- --------------------------------------------------------

--
-- Table structure for table `channel_customization`
--

CREATE TABLE `channel_customization`
(
    `id`          int(11)    NOT NULL,
    `image_id`    int(11)    NOT NULL,
    `description` text,
    `created_at`  bigint(20) NOT NULL,
    `updated_at`  bigint(20) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = cp1251;

-- --------------------------------------------------------

--
-- Table structure for table `channel_member`
--

CREATE TABLE `channel_member`
(
    `id`             int(11)    NOT NULL,
    `channel_id`     int(11)    NOT NULL,
    `member_id`      int(11)    NOT NULL,
    `member_role_id` int(11)    NOT NULL,
    `created_at`     bigint(20) NOT NULL,
    `updated_at`     bigint(20) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = cp1251;

-- --------------------------------------------------------

--
-- Table structure for table `channel_member_role`
--

CREATE TABLE `channel_member_role`
(
    `id`   int(11)     NOT NULL,
    `name` varchar(65) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = cp1251;

-- --------------------------------------------------------

--
-- Table structure for table `channel_post`
--

CREATE TABLE `channel_post`
(
    `id`         int(11)    NOT NULL,
    `channel_id` int(11)    NOT NULL,
    `content`    text       NOT NULL,
    `created_at` bigint(20) NOT NULL,
    `updated_at` bigint(20) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = cp1251;

-- --------------------------------------------------------

--
-- Table structure for table `channel_post_attachment`
--

CREATE TABLE `channel_post_attachment`
(
    `id`              int(11) NOT NULL,
    `channel_post_id` int(11) NOT NULL,
    `attachment_id`   int(11) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = cp1251;

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat`
(
    `id`         int(11)    NOT NULL,
    `creator_id` int(11)    NOT NULL,
    `created_at` bigint(20) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = cp1251;

-- --------------------------------------------------------

--
-- Table structure for table `chat_member`
--

CREATE TABLE `chat_member`
(
    `id`        int(11)    NOT NULL,
    `chat_id`   int(11)    NOT NULL,
    `member_id` int(11)    NOT NULL,
    `joined_at` bigint(20) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = cp1251;

-- --------------------------------------------------------

--
-- Table structure for table `chat_member_message`
--

CREATE TABLE `chat_member_message`
(
    `id`             int(11)    NOT NULL,
    `chat_id`        int(11)    NOT NULL,
    `chat_member_id` int(11)    NOT NULL,
    `content`        text       NOT NULL,
    `created_at`     bigint(20) NOT NULL,
    `updated_at`     bigint(20) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = cp1251;

-- --------------------------------------------------------

--
-- Table structure for table `chat_member_message_attachment`
--

CREATE TABLE `chat_member_message_attachment`
(
    `id`                     int(11) NOT NULL,
    `chat_member_message_id` int(11) NOT NULL,
    `attachment_id`          int(11) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = cp1251;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role`
(
    `id`   int(11)     NOT NULL,
    `name` varchar(65) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = cp1251;

-- --------------------------------------------------------

--
-- Table structure for table `upload`
--

CREATE TABLE `upload`
(
    `id`                 int(11)      NOT NULL,
    `upload_category_id` int(11)      NOT NULL,
    `path`               varchar(255) NOT NULL,
    `name`               varchar(65)  NOT NULL,
    `extension`          varchar(8)   NOT NULL,
    `size`               int(11)      NOT NULL,
    `created_at`         bigint(20)   NOT NULL,
    `updated_at`         bigint(20)   NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = cp1251;

-- --------------------------------------------------------

--
-- Table structure for table `upload_category`
--

CREATE TABLE `upload_category`
(
    `id`   int(11)     NOT NULL,
    `name` varchar(65) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = cp1251;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user`
(
    `id`             int(11)     NOT NULL,
    `avatar_id`      int(11)              DEFAULT NULL,
    `name`           varchar(65) NOT NULL,
    `email`          varchar(65) NOT NULL,
    `login`          varchar(65) NOT NULL,
    `password`       varchar(65) NOT NULL,
    `local_password` varchar(8)           DEFAULT NULL,
    `about`          text,
    `is_enabled`     tinyint(1)  NOT NULL DEFAULT '0',
    `created_at`     bigint(20)  NOT NULL,
    `updated_at`     bigint(20)  NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = cp1251;

-- --------------------------------------------------------

--
-- Table structure for table `user_blacklist`
--

CREATE TABLE `user_blacklist`
(
    `id`              int(11)    NOT NULL,
    `user_id`         int(11)    NOT NULL,
    `user_blocked_id` int(11)    NOT NULL,
    `blocked_at`      bigint(20) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = cp1251;

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role`
(
    `id`      int(11) NOT NULL,
    `user_id` int(11) NOT NULL,
    `role_id` int(11) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = cp1251;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `channel`
--
ALTER TABLE `channel`
    ADD PRIMARY KEY (`id`),
    ADD KEY `FK_channel__channel_customization` (`channel_customization_id`);

--
-- Indexes for table `channel_customization`
--
ALTER TABLE `channel_customization`
    ADD PRIMARY KEY (`id`),
    ADD KEY `FK_channel_customization__upload` (`image_id`);

--
-- Indexes for table `channel_member`
--
ALTER TABLE `channel_member`
    ADD PRIMARY KEY (`id`),
    ADD KEY `FK_channel_member__channel` (`channel_id`),
    ADD KEY `FK_channel_member__channel_member_role` (`member_role_id`),
    ADD KEY `FK_channel_member__user` (`member_id`);

--
-- Indexes for table `channel_member_role`
--
ALTER TABLE `channel_member_role`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `channel_post`
--
ALTER TABLE `channel_post`
    ADD PRIMARY KEY (`id`),
    ADD KEY `FK_channel_post__channel` (`channel_id`);

--
-- Indexes for table `channel_post_attachment`
--
ALTER TABLE `channel_post_attachment`
    ADD PRIMARY KEY (`id`),
    ADD KEY `FK_channel_post_attachment__channel_post` (`channel_post_id`),
    ADD KEY `FK_channel_post_attachment__upload` (`attachment_id`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
    ADD PRIMARY KEY (`id`),
    ADD KEY `FK_chat__user` (`creator_id`);

--
-- Indexes for table `chat_member`
--
ALTER TABLE `chat_member`
    ADD PRIMARY KEY (`id`),
    ADD KEY `FK_chat_member__chat` (`chat_id`),
    ADD KEY `FK_chat_member__user` (`member_id`);

--
-- Indexes for table `chat_member_message`
--
ALTER TABLE `chat_member_message`
    ADD PRIMARY KEY (`id`),
    ADD KEY `FK_chat_member_message__chat` (`chat_id`),
    ADD KEY `FK_chat_member_message__chat_member` (`chat_member_id`);

--
-- Indexes for table `chat_member_message_attachment`
--
ALTER TABLE `chat_member_message_attachment`
    ADD PRIMARY KEY (`id`),
    ADD KEY `FK_chat_member_message_attachment__chat_member_message` (`chat_member_message_id`),
    ADD KEY `FK_chat_member_message_attachment__upload` (`attachment_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `upload`
--
ALTER TABLE `upload`
    ADD PRIMARY KEY (`id`),
    ADD KEY `FK_upload__upload_category` (`upload_category_id`);

--
-- Indexes for table `upload_category`
--
ALTER TABLE `upload_category`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
    ADD PRIMARY KEY (`id`),
    ADD KEY `FK_user__upload` (`avatar_id`);

--
-- Indexes for table `user_blacklist`
--
ALTER TABLE `user_blacklist`
    ADD PRIMARY KEY (`id`),
    ADD KEY `FK_user_blacklist__user` (`user_id`),
    ADD KEY `FK_user_blacklist__user__blocked` (`user_blocked_id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
    ADD PRIMARY KEY (`id`),
    ADD KEY `FK_user_role__user` (`user_id`),
    ADD KEY `FK_user_role__role` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `channel`
--
ALTER TABLE `channel`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `channel_customization`
--
ALTER TABLE `channel_customization`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `channel_member`
--
ALTER TABLE `channel_member`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `channel_member_role`
--
ALTER TABLE `channel_member_role`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `channel_post`
--
ALTER TABLE `channel_post`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `channel_post_attachment`
--
ALTER TABLE `channel_post_attachment`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chat_member`
--
ALTER TABLE `chat_member`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chat_member_message`
--
ALTER TABLE `chat_member_message`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chat_member_message_attachment`
--
ALTER TABLE `chat_member_message_attachment`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `upload`
--
ALTER TABLE `upload`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `upload_category`
--
ALTER TABLE `upload_category`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_blacklist`
--
ALTER TABLE `user_blacklist`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `channel`
--
ALTER TABLE `channel`
    ADD CONSTRAINT `FK_channel__channel_customization` FOREIGN KEY (`channel_customization_id`) REFERENCES `channel_customization` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `channel_customization`
--
ALTER TABLE `channel_customization`
    ADD CONSTRAINT `FK_channel_customization__upload` FOREIGN KEY (`image_id`) REFERENCES `upload` (`id`);

--
-- Constraints for table `channel_member`
--
ALTER TABLE `channel_member`
    ADD CONSTRAINT `FK_channel_member__channel` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `FK_channel_member__channel_member_role` FOREIGN KEY (`member_role_id`) REFERENCES `channel_member_role` (`id`),
    ADD CONSTRAINT `FK_channel_member__user` FOREIGN KEY (`member_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `channel_post`
--
ALTER TABLE `channel_post`
    ADD CONSTRAINT `FK_channel_post__channel` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `channel_post_attachment`
--
ALTER TABLE `channel_post_attachment`
    ADD CONSTRAINT `FK_channel_post_attachment__channel_post` FOREIGN KEY (`channel_post_id`) REFERENCES `channel_post` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `FK_channel_post_attachment__upload` FOREIGN KEY (`attachment_id`) REFERENCES `upload` (`id`);

--
-- Constraints for table `chat`
--
ALTER TABLE `chat`
    ADD CONSTRAINT `FK_chat__user` FOREIGN KEY (`creator_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `chat_member`
--
ALTER TABLE `chat_member`
    ADD CONSTRAINT `FK_chat_member__chat` FOREIGN KEY (`chat_id`) REFERENCES `chat` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `FK_chat_member__user` FOREIGN KEY (`member_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `chat_member_message`
--
ALTER TABLE `chat_member_message`
    ADD CONSTRAINT `FK_chat_member_message__chat` FOREIGN KEY (`chat_id`) REFERENCES `chat` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `FK_chat_member_message__chat_member` FOREIGN KEY (`chat_member_id`) REFERENCES `chat_member` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `chat_member_message_attachment`
--
ALTER TABLE `chat_member_message_attachment`
    ADD CONSTRAINT `FK_chat_member_message_attachment__chat_member_message` FOREIGN KEY (`chat_member_message_id`) REFERENCES `chat_member_message` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `FK_chat_member_message_attachment__upload` FOREIGN KEY (`attachment_id`) REFERENCES `upload` (`id`);

--
-- Constraints for table `upload`
--
ALTER TABLE `upload`
    ADD CONSTRAINT `FK_upload__upload_category` FOREIGN KEY (`upload_category_id`) REFERENCES `upload_category` (`id`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
    ADD CONSTRAINT `FK_user__upload` FOREIGN KEY (`avatar_id`) REFERENCES `upload` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `user_blacklist`
--
ALTER TABLE `user_blacklist`
    ADD CONSTRAINT `FK_user_blacklist__user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `FK_user_blacklist__user__blocked` FOREIGN KEY (`user_blocked_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_role`
--
ALTER TABLE `user_role`
    ADD CONSTRAINT `FK_user_role__role` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
    ADD CONSTRAINT `FK_user_role__user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;