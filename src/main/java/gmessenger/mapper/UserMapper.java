package gmessenger.mapper;

import gmessenger.dto.UserDto;
import gmessenger.entity.User;
import gmessenger.entity.UserRole;

import java.util.ArrayList;
import java.util.List;

/**
 * UserMapper class
 */
public class UserMapper {
    /**
     * Convert {@User} to {@UserDto}
     *
     * @param user User
     * @return UserDto
     */
    public static UserDto userToUserDto(User user) {
        List<UserRole> roles = user.getRoles();

        List<String> rolesArray = new ArrayList<>();
        if (roles != null &&!roles.isEmpty()) {
            roles.forEach(role -> {
                rolesArray.add(role.getRole().getName());
            });
        }

        return new UserDto(
                user.getId(),
                user.getName(),
                user.getEmail(),
                user.getLogin(),
                user.getAbout(),
                "test",
                rolesArray,
                user.isEnabled(),
                user.getCreatedAt(),
                user.getUpdatedAt()
        );
    }
}
